<!DOCTYPE html>
<html lang="de">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bestellofix.com! </title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>

<body>

    <?php
    /*
// GET BY ID
    require_once('domain/Book.php');

    $bookById = Book::getById(7);

    echo $bookById['title'];
    */
    ?>

    <nav class="navbar navbar-expand-lg navbar-light bg-warning">
        <div class="container">
            <h1>Bestellofix.com!</h1>
            <ul class="navbar-nav ml-auto">
                <!-- Badge -->
                <li class="nav-item">
                    <a class="nav-link" href="cartcheckout.php">
                        <span><img src="/img/cart4.svg" alt="Warenkorb" width="32" height="32"></span>
                        <span class="badge badge-pill bg-danger">
                            <?php require_once('domain/Cart.php');
                            $amount = Cart::getAmountOfItemsInCart();
                            echo $amount; ?>
                        </span>
                    </a>
                </li>
            </ul>
        </div>
    </nav>

    <div class="container-fluid">
        <hr class="border-2 border-top border-warning">
        <?php
        // GET ALL
        require_once('domain/Book.php');

        $allBooks = Book::getAll();

        foreach ($allBooks as $book) {
            $id = $book['id'];
            $title = $book['title'];
            $price = $book['price'];
            $stock = $book['stock'];

            $productContainer = '
            <form action="cartcheckout.php" method="post">
            <div class="container" style="border:1px solid #cecece;">
            <div class="row">
                <div class="col-sm-3">
                    <b>' . $title . '</b>
                </div>
                <div class="col-sm-3">
                    € ' . $price . '
                </div>
                <div class="col-sm-3">
                    <select name="amount" id="amount" class="form-select">';

            // Setzt die Maximale Auswahl an Artikeln auf die Anzahl vorrätig
            for ($i = 1; $i <= $stock; $i++) {
                $productContainer .= '<option value=' . $i . '>' . $i . '</option>';
            }

            // Wenn Vorrat auf 0
            if ($stock > 0) {
                $productContainer .=
                    '<option selected>Menge...</option>
                </select>
                </div>
                <td>
                    
                        <button name="bookid" value=' . $id . ' type="submit" class="btn btn-warning">hinzufügen</button>
                    </form>
                </td>
                </div>
                </div>
                </div>
                </div>
                <br>';
            } else {
                $productContainer .=
                    '<option selected>nicht Verfügbar</option>
                </select>
                </div>
                </div>
                </div>
                </div>
                <br>';
            }

            echo $productContainer;
        }
        ?>
</body>

<footer class="bg-light text-center text-lg-start navbar-fixed-bottom">
    <div class="text-center p-3 text-dark" style="background-color: rgba(0, 0, 0, 0.2);">
        © 2022 Copyright:
        <a class="text-warning" href="/">Bestellofix.com</a>
    </div>
</footer>

</html>