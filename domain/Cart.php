<?php
class Cart
{
    public static $instance;

    public function __construct()
    {
        self::$instance = $this;
    }

    public static function getActiveCart()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function getAllCartItems()
    {
        return $this->loadCookie();
    }

    private function loadCookie()
    {
        // String wieder in ein Array packen
        if (isset($_COOKIE['cart'])) {
            $myCartItems = unserialize($_COOKIE['cart']);
        } else {
            $myCartItems = [];
        }

        return $myCartItems;
    }

    private function saveCookie($book)
    {
        // Lade alle vorhandenen Items im Warenkorb
        $cookieCartItems = [];
        $cookieCartItems = $this->loadCookie();
        $cookie_name = "cart";

        // Falls vorher noch kein Cookie gesetzt war
        if ($cookieCartItems == null) {
            setcookie($cookie_name, "empty", time() + 86000, '/');
            $cookieCartItems = $this->loadCookie();
        }

        // Schiebe das neue Buch in den (vorhandenen) Warenkorb
        array_push($cookieCartItems, $book);

        // Konvertiere Array in einen String, um es im Cookie abspeichern zu können
        setcookie($cookie_name, serialize($cookieCartItems), time() + 86000, '/');
    }

    public function add($book, $count)
    {
        //$cartitems = $this->loadCookie();

        // Überprüfen, ob Item schon im Warenkorb existiert
        /*
        foreach($cartitems as $item){
            if($item['id'] === $book->getId()){
                return true;
            }else{
                return false;
            }
        }
        */

        $array = array($book->getTitle() => $count);
        
        echo $array['Bamity'];

        $this->saveCookie($book);
    }

    public function getCount(){

    }

    public function remove($id)
    {
        //$this->saveCookie();
    }

    public static function getAmountOfItemsInCart()
    {
        if (isset($_COOKIE['cart'])) {
            $myCartItems = unserialize($_COOKIE['cart']);
        } else {
            $myCartItems = [];
        }
        return count($myCartItems);
    }

    public function isBookAlreadyInCart($book){
        $cartitems = $this->loadCookie();
        
        foreach($cartitems as $item){
            if($item['id'] === $book->getId()){
                return true;
            }else{
                return false;
            }
        }

    }
}
