<?php
class Book{
    private int $bid;
    private string $btitle;
    private float $bprice;
    private int $bstock;

    public function __construct($bid, $btitle, $bprice, $bstock)
    {
        $this->bid = $bid;
        $this->btitle = $btitle;
        $this->bprice = $bprice;
        $this->bstock = $bstock;
    }

    public static function getAll(){
        $data = file_get_contents("data/PHP-23 bookdata.json");
        $data = json_decode($data, true); // true returns an array
        return $data;
    }

    public static function getById($id){
        $data = file_get_contents("data/PHP-23 bookdata.json");
        $data = json_decode($data, true);

        foreach ($data as $book) {
            if($book['id'] == $id){
                return $book;
            }
        }
    }

    public function getId(){
        return $this->bid;
    }

    public function getTitle(){
        return $this->btitle;
    }

    public function getPrice(){
        return $this->bprice;
    }

    public function getStock(){
        return $this->bstock;
    }
}
?>