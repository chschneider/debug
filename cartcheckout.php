<!DOCTYPE html>
<html lang="de">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bestellofix.com! </title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-warning">
        <div class="container">
            <h1>Bestellofix.com!</h1>
            <ul class="navbar-nav ml-auto">
                <!-- Badge -->
                <li class="nav-item">
                    <a class="nav-link" href="cartcheckout.php">
                        <span><img src="/img/cart4.svg" alt="Warenkorb" width="32" height="32"></span>
                        <span class="badge badge-pill bg-danger">
                            <?php
                            require_once('domain/Cart.php');
                            $amount = Cart::getAmountOfItemsInCart();
                            echo $amount;
                            ?>
                        </span>
                    </a>
                </li>
            </ul>
        </div>
    </nav>

    <div class="container-fluid">
        <hr class="border-2 border-top border-warning">
        <?php
        require_once('domain/Cart.php');
        require_once('domain/Book.php');

        // Wenn es keinen Warenkorb gibt und nichts darin liegt - erstelle einen neuen
        if (!isset($_COOKIE['cart'])) {
            echo '<div class="container">';
            echo "<h4>Ihr Warenkorb ist leer!</h4><br>";
            echo '<a href="index.php" class="btn btn-warning">< weiter einkaufen</a>';
            echo '</div>';
            $cartObject = new Cart();
        } else {
            $cartObject = Cart::getActiveCart();

            $getAllCartItems = $cartObject->getAllCartItems();

            if (isset($_POST['amount'])) {
                $amountOfProducts = $_POST['amount'];
            }else{
                $amountOfProducts = 0;
            }

            foreach ($getAllCartItems as $item) {

                $cartItemContainer = '
            <div class="container" style="border:1px solid #cecece;">
            <div class="row">
                <div class="col-sm-3">
                    <b>' . $item->getTitle() . '</b>
                </div>
                <div class="col-sm-3">
                    € ' . $item->getPrice() . '
                </div>
                <div class="col-sm-3">
                    ' . $amountOfProducts . '
                </div>
                </div>
                </div>
                </div>
                </div>
                <br>';

                echo $cartItemContainer;
            }
            echo '<div class="container">';
            echo '<a href="index.php" class="btn btn-warning">< weiter einkaufen</a>';
            echo '</div>';
        }

        if (isset($_POST['bookid'])) {
            $id = $_POST['bookid'];
            $bookById = Book::getById($id);

            $id = array_values($bookById)[0];
            $title = array_values($bookById)[1];
            $price = array_values($bookById)[2];
            $stock = array_values($bookById)[3];

            $bookObject = new Book($id, $title, $price, $stock);

            $cartObject->add($bookObject, 1);

            unset($_POST);
            $_POST = array();
            header("Location: cartcheckout.php");
        }
        ?>
    </div>
</body>

<footer class="bg-light text-center text-lg-start mt-3">
    <div class="text-center p-3 text-dark" style="background-color: rgba(0, 0, 0, 0.2);">
        © 2022 Copyright:
        <a class="text-warning" href="/">Bestellofix.com</a>
    </div>
</footer>

</html>